var mongoose = require('mongoose');

var Todo = mongoose.model('Todo', {
  user: {
    type: String,
    required: true,
    minlength: 1,
    trim: true
  },
  langitude: {
    type: Boolean,
    default: false
  },
  latitude: {
    type: Number,
    default: null
  }
});

module.exports = {Todo};
