 // const MongoClient = require('mongodb').MongoClient;
const {MongoClient, ObjectID} = require('mongodb');

 
MongoClient.connect('mongodb://localhost:27017/TodoApp', (err, db) => {
  if (err) {
    return console.log('Unable to connect to MongoDB server');
  }
  console.log('Connected to MongoDB server');
  
  //findOneAndUpdate
  /*db.collection('Users').findOneAndUpdate({
	 _id: new ObjectID('5b7b3f96ec123012f8b3958c')
  }, {
	  $set: {
		  age: 25
	  }
  }, {
	  returnOriginal: false
  }).then((result) => {
	  console.log(result);
  });
  */
  
  //Updating and increment by 1
  db.collection('Users').findOneAndUpdate({
	 _id: new ObjectID('5b7b3f96ec123012f8b3958c')
  }, {
	  $set: {
		  name: 'Rakesh kumar'
	  },
	  $inc: {
		  age:1
	  }
	 }, {
	  returnOriginal: false
  }).then((result) => {
	  console.log(result);
  });
  
  //db.close();
});