// const MongoClient = require('mongodb').MongoClient;
const {MongoClient, ObjectID} = require('mongodb');

/*var obj = new ObjectID();
console.log(obj);
*/

 /*var user = {name: 'andrew', age: 25};
 var {name} = user;
 console.log(name);
 */
 
MongoClient.connect('mongodb://localhost:27017/TodoApp', (err, db) => {
  if (err) {
    return console.log('Unable to connect to MongoDB server');
  }
  console.log('Connected to MongoDB server');

  /*db.collection('Users').insertOne({
    name: 'Jam',
    age: 27,
    location: 'Pune'
  }, (err, result) => {
    if (err) {
      return console.log('Unable to insert user', err);
    }

    console.log(result.ops[0]._id.getTimestamp());
  });*/

  db.close();
});