 // const MongoClient = require('mongodb').MongoClient;
const {MongoClient, ObjectID} = require('mongodb');

 
MongoClient.connect('mongodb://localhost:27017/TodoApp', (err, db) => {
  if (err) {
    return console.log('Unable to connect to MongoDB server');
  }
  console.log('Connected to MongoDB server');
  
  //deleteMany
  /*db.collection('Users').deleteMany({name: 'Jam'}).then((result) => {
	  console.log(result);
0  });
  */
  
  //deleteOne
  /*db.collection('Users').deleteOne({name: 'Jam'}).then((result) => {
	  console.log(result);
  });
  */
  
  //findOneAndDelete
  /*db.collection('Users').findOneAndDelete({name: 'Jam'}).then((result) => {
	  console.log(result);
  });
   */
  
  //db.close();
});